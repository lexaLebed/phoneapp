/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        this.watchID = null;
    },

    bindEvents: function() {
        var _this = this;

        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);

        $('#camera').on('click', function(){
            _this.changePicture();
        });

        $('#locationShow').on('click', function(){
            _this.locationShow();
        });

        $('#accel').on('click', function(){
            _this.accel();
        });

        $('#stopWatch').on('click', function(){
            _this.stopWatch();
        });
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {

        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');


        console.log('Received Event: ' + id);
    },

    changePicture: function() {
        if (!navigator.camera) {
            alert("Camera API not supported", "Error");
            return;
        }
        var options =   {   quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
            encodingType: 0     // 0=JPG 1=PNG
        };

        navigator.camera.getPicture(
            function(imgData) {
                $('#image').attr('src', "data:image/jpeg;base64," + imgData);
            },
            function() {
                alert($('#image'))
                alert('Error taking picture', 'Error');
            },
            options);

        return false;
    },

    locationShow: function() {
        var onSuccess = function(position) {
            alert('Latitude: '          + position.coords.latitude          + '\n' +
                  'Longitude: '         + position.coords.longitude         + '\n' +
                  'Altitude: '          + position.coords.altitude          + '\n' +
                  'Accuracy: '          + position.coords.accuracy          + '\n' +
                  'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
                  'Heading: '           + position.coords.heading           + '\n' +
                  'Speed: '             + position.coords.speed             + '\n' +
                  'Timestamp: '         + position.timestamp                + '\n');
        };



        // onError Callback receives a PositionError object
        //
        function onError(error) {
            alert('code: '    + error.code    + '\n' +
                  'message: ' + error.message + '\n');
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError);    
    },

    accel: function() {
        var options = { frequency: 300 };

        this.watchID = navigator.accelerometer.watchAcceleration(onSuccess, onError, options);

            var ball   = document.querySelector('.ball');
            var garden = document.querySelector('.garden');
            var output = document.querySelector('.output');

            var maxX = garden.clientWidth  - ball.clientWidth;
            var maxY = garden.clientHeight - ball.clientHeight;

        function onSuccess(acceleration) {
            /*alert('Acceleration X: ' + acceleration.x + '\n' +
                  'Acceleration Y: ' + acceleration.y + '\n' +
                  'Acceleration Z: ' + acceleration.z + '\n' +
                  'Timestamp: '      + acceleration.timestamp + '\n');*/



           // function handleOrientation() {
              var x = acceleration.x * 10;  // In degree in the range [-180,180]
              var y = acceleration.y * 10;  // In degree in the range [-90,90]

              output.innerHTML  = "beta : " + x + "\n";
              output.innerHTML += "gamma: " + y + "\n";

              // Because we don't want to have the device upside down
              // We constrain the x value to the range [-90,90]
              if (x >  90) { x =  90};
              if (x < -90) { x = -90};

              // To make computation easier we shift the range of 
              // x and y to [0,180]
              x += 90;
              y += 90;

              // 10 is half the size of the ball
              // It center the positioning point to the center of the ball
              ball.style.left  = (maxX*x/180 - 10) + "px";
              ball.style.top = (maxY*y/180 - 10) + "px";
            //}

           //  window.addEventListener('deviceorientation', handleOrientation);
             //window.addEventListener("devicemotion", handleOrientation, true);
        }



        function onError() {
            alert('onError!');
        }

       // navigator.accelerometer.getCurrentAcceleration(onSuccess, onError);
    },

    stopWatch: function() {
        if (this.watchID) {
            navigator.accelerometer.clearWatch(this.watchID);
            this.watchID = null;
        }
    }


};

app.initialize();